<?php

use \Tamtamchik\SimpleFlash\Flash;

/**
 * Rendu de l'index
 */
function index() {
    setlocale(LC_TIME, "fr_FR", "French");
    if(Flight::request()->method == 'POST'){
        //
    }
    $data = [

    ];
    return $data;
}

/**
 * rend la page /about
 */
function about(){
    $data = [];
    Flight::render('about.twig', $data);
}